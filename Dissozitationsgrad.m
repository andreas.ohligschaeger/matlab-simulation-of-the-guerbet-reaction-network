function [cdiss] = Dissozitationsgrad(cH2O,cEtOH,T,cSalz)
if cSalz == 0
    cdiss = 0;
else
    R       = 8.314461;                             %in J/mol/K
    S       = -133.89;                              %in J/mol/K
    HEtOH   = -22941.60978;                         %in J/mol
    HH2O    = -33177.38308;                         %in J/mol                     
    xEtOH   = (cEtOH)/(cEtOH+cH2O);
    xH2O    = (cH2O)/(cEtOH+cH2O);
    H       = xEtOH*HEtOH + xH2O*HH2O;              %in J/mol
    K       = exp(-(H-T*S)/(R*T));                  
    cdiss = (sqrt(4*K*cSalz+K^2)-K)/(2*cSalz);      %in mol/L da H und S auf 1 mol/L referenziert sind 
end
end