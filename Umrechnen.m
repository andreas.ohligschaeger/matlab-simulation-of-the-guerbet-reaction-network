function [DatensatzmitKOH] = Umrechnen(a)
s = size(a{1,2});%Zeilen ,Spalten
d = a{1,2};
g = zeros(s(1,1),1);
b = [d, g];
for i = 1:s(1,1)
    EtOH=b(i,3);
    EtO=b(i,6);
    H2O=b(i,8);
    T = b(1,15);
    [cOH, cEtOH, cH2O] = Ethanolatgg(EtOH, H2O, EtO, T);
    cEtO = EtO-cOH;
    b(i,3)= cEtOH;
    b(i,6)= cEtO;
    b(i,8)= cH2O;
    b(i,17)= cOH;    
end
DatensatzmitKOH = b;

plot(a{1,1}/3600,b(:,3:6));
%plot(a{1,1}/3600,b(:,4:6));
hold on
plot(a{1,1}/3600,b(:,17));
plot(a{1,1}/3600,b(:,7:8));
plot(a{1,1}/3600,b(:,9),'--');
plot(a{1,1}/3600,b(:,10:14),'--');
%legend('AcH','EtOAc','KOEt','KOH','KOAc','H_2O','CrAl','C6 Oligomer','C8 Oligomer','CrOH','BuAl','BuOH','Fontsize',20)
legend('EtOH','AcH','EtOAc','KOEt','KOH','KOAc','H_2O','CrAl','C6 Oligomer','C8 Oligomer','CrOH','BuAl','BuOH','Fontsize',20)
xlabel('Zeit {\it t} [h]','Fontsize',18)
ylabel('Konzentrationen [i] [mol L^-^1]','Fontsize',18)
set(gca,'FontSize',16)
end
%um Datahints gr��er zu machen
%alldatacursors = findall(gcf,'type','hggroup')
%set(alldatacursors,'FontSize',12)
