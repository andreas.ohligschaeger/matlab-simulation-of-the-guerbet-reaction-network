function [cH2] = pH2incH2(T,pH2,cges,mges,cKOEt,cKOAc)
b       = 1.055958526;                           %Verh�ltnis von Poynting Korrekturfaktor und vapor-phase fugacity coefficient gemittelt
HEtOH   = exp(-52.6234E-8*T^3 + 63.272E-5*T^2 + -25.858E-2*T + 40.599); %Henrykonstanten
xH2salzfrei   = pH2/(HEtOH*b);                   %Henry Gleichung der Quelle
wSalz   = (cKOEt*84.16+cKOAc*98.15)/mges;        %Massenanteil der Salze
xH2     = exp(-4.3131*wSalz)*xH2salzfrei;        %Wasserstoffanteil in der L�sung mit Salz
cH2     = (xH2*cges)/(1-xH2);                     %Konzentration in mol/L
end     
%xH2     = xH2salzfrei*(-0.309/0.095*wSalz+1)%xH2salzfrei*(-0.056/0.045*wKOAc+1)*(-0.635/0.05*wKOH+1);    %Salzeffekt xH2salzfrei*(-0.309/0.095*wSalz+1);