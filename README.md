Alle MatLabbskripte müssen in einem Ordner sein. 
Zusätzlich muss ein Ordner zum Speichern der Zwischenergebnisse erstellte werden.
In den MatLab-Skripten müssen diverse Dateipfade geändert werden. 
ODESolver.m: Im save-Befehl den Dateipfad zu den Skripten angeben.
Plot.m und Maximum.m: Im load-Befehl den Dateipfad zu den Skripten angeben.
parsave.m: Im save-Befehl den Dateipfad des Ordners mit den Zwischenergebnissen angeben.
parload.m: Im load-Befehl den Dateipfad des Ordners mit den Zwischenergebnissen angeben.

Zu installierende Add-ons: Parallel Computing Toolbox, Scatterbar3. 
(konfigurieren der Parallel Computing Toolbox über Home -> Parallel -> Parallel Preference 
-> Kerne und Threads zuweisen und anschließend unter select a default cluster auswählen)

Input Excel Tabelle muss im Ordner der MatLAb-Skripte sein

Als Solver bieten sich ode15s/ode13tb oder ode13s an (ODESolver.m Zeile 16)

Die einzelnen c(t)-Verläufe werden in einem 4D-Cell-Array gespeichert. 
Die Koordinaten in der Tabelle berechnen sich aus Betriebparametern (Datensatz{Temp,Druck,AcO,H2O})

Beispiel zu Berechnung einer Koordinate: 320K (Starttemperatur von 300K und 5K Schrittgröße)
(320K-300K)/5+1=5 -> Temp=5 für Simulationen bei 320K

Die übrigen Skripta sind dazu gedacht, den Output der Simulation weiter auszuwerten..
