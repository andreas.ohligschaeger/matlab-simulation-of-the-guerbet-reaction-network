function [KOH,EtOH,H2O] = Ethanolatgg(cEtOH, cH2O, Base, T)%EtOH+OH-<=>EtO-+H2O Bass gleich ethanolat
if cH2O == 0
    KOH= 0;
    EtOH = cEtOH;
    H2O = cH2O;
else 
    R  = 8.314461;   %in J/mol/K
    S  = 60;         %in J/mol/K
    pKaWasser = 14;
    pKaEtOH = 16;
    K0 = 10^(pKaWasser-pKaEtOH);
    G0 = -R*298*log(K0);
    G  = G0-S*(T-298); %in J/mol
    K  = exp(-G/R/T);
    syms cKOH positive
    %Ethanolat = -((-cH2O-K*cKOH-K*cEtOH)/(K-1))/2+sqrt((((-cH2O-K*cKOH-K*cEtOH)/(K-1))/2)^2-(K*cEtOH*cKOH)/(K-1));
    %cOH = solve(      ((Base-cOH)*(cH2O-cOH))/((cEtOH+cOH)*cOH)      ,cOH);
    KOH1 = -((K*cEtOH+cH2O+Base)/(2*K-2))-sqrt(((K*cEtOH+cH2O+Base)/(2*K-2))^2+(cH2O*Base)/(K-1));
    %KOH1 = solve(K == ((Base-cKOH)*(cH2O-cKOH))/((cEtOH+cKOH)*(cKOH)),cKOH);
    KOH = double(KOH1(1,1));
    EtOH = cEtOH+KOH; 
    H2O = cH2O-KOH;
end
end
