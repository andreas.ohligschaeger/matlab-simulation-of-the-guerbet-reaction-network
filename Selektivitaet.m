function [S1, S2, S3] = Selektivitaet(c)
Datensatz = Umrechnen(c);
x = Datensatz;
s = size(x);%s(1,1)
S1 = (2*Datensatz(s(1,1),14))/((14+Datensatz(1,6)-Datensatz(s(1,1),6))-(Datensatz(s(1,1),3)+2*Datensatz(s(1,1),5)));%BuOH
S2 = (4*Datensatz(s(1,1),11))/((14+Datensatz(1,6)-Datensatz(s(1,1),6))-(Datensatz(s(1,1),3)+2*Datensatz(s(1,1),5)));%C8 Oligomer
S3 = (Datensatz(s(1,1),7))/((14+Datensatz(1,6)-Datensatz(s(1,1),6))-(Datensatz(s(1,1),3)+2*Datensatz(s(1,1),5)));%KoAc
S = S1+S2+S3%ca 1 zur Kontrolle
end