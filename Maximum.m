load(fullfile('D:\Bachelorarbeit\MatLab', 'Ergebnisse'),'Datensatz');
Betriebsparameter = xlsread('Input.xlsx', 'Betriebsparameter');
%3D-Plots Konzentration
for H2O = 1:(Betriebsparameter(3,3)-Betriebsparameter(1,3))/Betriebsparameter(2,3)+1%Wasserausgangskonzentrationsschleife
    Wasser = (H2O-1)*Betriebsparameter(2,3)+Betriebsparameter(1,3);
    for AcO = 1:(Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1%Acetatausgangskonzentrationsschleife
        Acetat = (AcO-1)*Betriebsparameter(2,4)+Betriebsparameter(1,4);
        M = zeros(int16(((Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1)),14+4);
        for Temp = 1:(Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1%Temperaturschleife
            T=(Temp-1)*Betriebsparameter(2,2)+Betriebsparameter(1,2);
            for Druck = 1:((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1))+1%Druckschleife
                pH2=(Druck-1)*Betriebsparameter(2,1)+Betriebsparameter(1,1);
                i = int16(Druck + (Temp-1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1));
                s = size(Datensatz{Temp,Druck,AcO,H2O}{1,2});
                M(i,:) = [Datensatz{Temp,Druck,AcO,H2O}{1,2}(s(1,1),1:13),max(Datensatz{Temp,Druck,AcO,H2O}{1,2}(:,14)),Datensatz{Temp,Druck,AcO,H2O}{1,2}(s(1,1),15:16),Acetat,Wasser];%T pH2 rausnehmen da jetzt in Datensatz gespeichert
            end
        end
        j = AcO + (H2O-1)*((Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1);
        figure(j)
        scatter3(M(:,15),M(:,16),M(:,14),[],M(:,14));
        colorbar
        grid on
        set(gca,'FontSize',16)
        xlabel('Temperatur {\it T} [K]','Fontsize',18)
        ylabel('Wasserstoffdruck {\it p}_H_2 [MPa]','Fontsize',18)
        zlabel('Butanolkonzentration [mol L^-^1]','Fontsize',18)
        axis([373  433.0000         0.4   11.6000         0    0.5])
    end
end