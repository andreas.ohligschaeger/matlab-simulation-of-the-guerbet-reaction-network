load(fullfile('D:\Bachelorarbeit\MatLab', 'Ergebnisse'),'Datensatz');
Betriebsparameter = xlsread('Input.xlsx', 'Betriebsparameter');
%3D-Plots Konzentration
for H2O = 1:(Betriebsparameter(3,3)-Betriebsparameter(1,3))/Betriebsparameter(2,3)+1%Wasserausgangskonzentrationsschleife
    Wasser = (H2O-1)*Betriebsparameter(2,3)+Betriebsparameter(1,3);
    for AcO = 1:(Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1%Acetatausgangskonzentrationsschleife
        Acetat = (AcO-1)*Betriebsparameter(2,4)+Betriebsparameter(1,4);
        M = zeros(int16(((Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1)),14+4);
        for Temp = 1:(Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1%Temperaturschleife
            T=(Temp-1)*Betriebsparameter(2,2)+Betriebsparameter(1,2);
            for Druck = 1:((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1))+1%Druckschleife
                pH2=(Druck-1)*Betriebsparameter(2,1)+Betriebsparameter(1,1);
                i = int16(Druck + (Temp-1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1));
                s = size(Datensatz{Temp,Druck,AcO,H2O}{1,2});
                M(i,:) = [Datensatz{Temp,Druck,AcO,H2O}{1,2}(s(1,1),:),Acetat,Wasser];%T pH2 rausnehmen da jetzt in Datensatz gespeichert
            end
        end
        j = AcO + (H2O-1)*((Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1);
        figure(j)
        scatter3(M(:,15),M(:,16),M(:,14),[],M(:,14));
        colorbar
        grid on
        set(gca,'FontSize',16)
        xlabel('Temperatur {\it T} [K]','Fontsize',18)
        ylabel('Wasserstoffdruck {\it p}_H_2 [MPa]','Fontsize',18)
        zlabel('Butanolkonzentration [mol L^-^1]','Fontsize',18)
        axis([373  433.0000         0.4   11.6000         0    0.4])
    end
end

for H2O = 1:(Betriebsparameter(3,3)-Betriebsparameter(1,3))/Betriebsparameter(2,3)+1%Wasserausgangskonzentrationsschleife
    Wasser = (H2O-1)*Betriebsparameter(2,3)+Betriebsparameter(1,3);
    for AcO = 1:(Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1%Acetatausgangskonzentrationsschleife
        Acetat = (AcO-1)*Betriebsparameter(2,4)+Betriebsparameter(1,4);
        M = zeros(int16(((Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1)),14+4);
        for Temp = 1:(Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1%Temperaturschleife
            T=(Temp-1)*Betriebsparameter(2,2)+Betriebsparameter(1,2);
            for Druck = 1:((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1))+1%Druckschleife
                pH2=(Druck-1)*Betriebsparameter(2,1)+Betriebsparameter(1,1);
                i = int16(Druck + (Temp-1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1));
                s = size(Datensatz{Temp,Druck,AcO,H2O}{1,2});
                M(i,:) = [Datensatz{Temp,Druck,AcO,H2O}{1,2}(s(1,1),:),Acetat,Wasser];%T pH2 rausnehmen da jetzt in Datensatz gespeichert
            end
        end
        j = AcO + (H2O-1)*((Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1);
        figure(j+1)
        scatter3(M(:,15),M(:,16),M(:,7),[],M(:,7));
        colorbar
        grid on
        set(gca,'FontSize',16)
        xlabel('Temperatur {\it T} [K]','Fontsize',18)
        ylabel('Wasserstoffdruck {\it p}_H_2 [MPa]','Fontsize',18)
        zlabel('Kaliumacetat [mol L^-^1]','Fontsize',18)
        axis([373  433.0000         0.4   11.6000         0    2])
    end
end

for H2O = 1:(Betriebsparameter(3,3)-Betriebsparameter(1,3))/Betriebsparameter(2,3)+1%Wasserausgangskonzentrationsschleife
    Wasser = (H2O-1)*Betriebsparameter(2,3)+Betriebsparameter(1,3);
    for AcO = 1:(Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1%Acetatausgangskonzentrationsschleife
        Acetat = (AcO-1)*Betriebsparameter(2,4)+Betriebsparameter(1,4);
        M = zeros(int16(((Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1)),14+4);
        for Temp = 1:(Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1%Temperaturschleife
            T=(Temp-1)*Betriebsparameter(2,2)+Betriebsparameter(1,2);
            for Druck = 1:((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1))+1%Druckschleife
                pH2=(Druck-1)*Betriebsparameter(2,1)+Betriebsparameter(1,1);
                i = int16(Druck + (Temp-1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1));
                s = size(Datensatz{Temp,Druck,AcO,H2O}{1,2});
                M(i,:) = [Datensatz{Temp,Druck,AcO,H2O}{1,2}(s(1,1),:),Acetat,Wasser];%T pH2 rausnehmen da jetzt in Datensatz gespeichert
            end
        end
        j = AcO + (H2O-1)*((Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1);
        figure(j+2)
        scatter3(M(:,15),M(:,16),M(:,11)+M(:,10),[],M(:,11)+M(:,10));
        colorbar
        grid on
        set(gca,'FontSize',16)
        xlabel('Temperatur {\it T} [K]','Fontsize',18)
        ylabel('Wasserstoffdruck {\it p}_H_2 [MPa]','Fontsize',18)
        zlabel('Oligomerkonzentration [mol L^-^1]','Fontsize',18)
        axis([373  433.0000         0.4   11.6000         0    0.6])
    end
end

%p=10MPa T=377K
figure(j+3)
Umrechnen(Datensatz{3,25,1,1})

%p=0.8MPa T=391K
figure(j+4)
Umrechnen(Datensatz{10,2,1,1})

%p=10.4MPa T=413K
figure(j+5)
Umrechnen(Datensatz{21,26,1,1})

%p=1.6MPa T=425K
figure(j+6)
Umrechnen(Datensatz{27,4,1,1})

for H2O = 1:(Betriebsparameter(3,3)-Betriebsparameter(1,3))/Betriebsparameter(2,3)+1%Wasserausgangskonzentrationsschleife
    Wasser = (H2O-1)*Betriebsparameter(2,3)+Betriebsparameter(1,3);
    for AcO = 1:(Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1%Acetatausgangskonzentrationsschleife
        Acetat = (AcO-1)*Betriebsparameter(2,4)+Betriebsparameter(1,4);
        M = zeros(int16(((Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1)),14+4);
        for Temp = 1:(Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1%Temperaturschleife
            T=(Temp-1)*Betriebsparameter(2,2)+Betriebsparameter(1,2);
            for Druck = 1:((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1))+1%Druckschleife
                pH2=(Druck-1)*Betriebsparameter(2,1)+Betriebsparameter(1,1);
                i = int16(Druck + (Temp-1)*((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1));
                s = size(Datensatz{Temp,Druck,AcO,H2O}{1,2});
                M(i,:) = [Datensatz{Temp,Druck,AcO,H2O}{1,2}(s(1,1),:),Acetat,Wasser];%T pH2 rausnehmen da jetzt in Datensatz gespeichert
            end
        end
        j = AcO + (H2O-1)*((Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1);
        figure(j+7)
        scatter3(M(:,15),M(:,16),(14-M(:,3))/14,[],(14-M(:,3))/14);
        colorbar
        grid on
        set(gca,'FontSize',16)
        xlabel('Temperatur {\it T} [K]','Fontsize',18)
        ylabel('Wasserstoffdruck {\it p}_H_2 [MPa]','Fontsize',18)
        zlabel('Ethanolumsatz {\it X} [dimlos]','Fontsize',18)
        axis([373  433.0000         0.4   11.6000         0    1])
    end
end


%xlabel('Zeit {\it t} [h]','Fontsize',18)
%xlabel('Temperatur {\it T} [K]','Fontsize',18)
%ylabel('Konzentrationen [i] [mol L^-^1]','Fontsize',18)
%ylabel('Wasserstoffdruck {\it p}_H_2 [MPa]','Fontsize',18)
%zlabel('Butanolkonzentration [mol L^-^1]','Fontsize',18)
%legend('Ethanol','Acetaldehyd','Ethylacetat','Fontsize',20)
%set(gca,'FontSize',16)
%hAx.XLabel.PositionMode='auto'
%axis([0 20 0 1])
%legend('EtOH','AcH','EtOAc','KOH','KOAc','H_2O','CrAl','CrOH','BuAl','BuOH','Fontsize',20)
%plot(Datensatz{7,1,1,1}{1,1}/3600,Datensatz{7,1,1,1}{1,2}(:,3:9))
%plot(Datensatz{7,1,1,1}{1,1}/3600,Datensatz{7,1,1,1}{1,2}(:,11:13))
%plot(Datensatz{1,1,1,1}{1,1}/3600,Datensatz{1,1,1,1}{1,2}(:,3:9))
%hold on
%plot(Datensatz{1,1,1,1}{1,1}/3600,Datensatz{1,1,1,1}{1,2}(:,12:14))
%legend('EtOH','AcH','EtOAc','KOH','KOAc','H_2O','CrAl','CrOH','BuAl','BuOH','Fontsize',20)