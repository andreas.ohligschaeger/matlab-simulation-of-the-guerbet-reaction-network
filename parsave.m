function parsave(Temp,Druck,AcO,H2O,t,c)%Speichert Zwischenergebnisse
Speicher{Temp,Druck,AcO,H2O} = {t,c};
ERG = Speicher;
Name = 'Results_T=%dpH2=%dAcO=%dH2O=%d';
fname = sprintf(Name,Temp,Druck,AcO,H2O);
save(fullfile('D:\Bachelorarbeit\MatLab\Zwischenerg', fname),'ERG');
end