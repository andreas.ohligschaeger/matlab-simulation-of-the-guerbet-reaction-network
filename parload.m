function [Gesamtergebnis] = parload(~) %L�d alle Zwischenergebnisse in ein cell array
Betriebsparameter = xlsread('Input.xlsx', 'Betriebsparameter');
Name = 'Results_T=%dpH2=%dAcO=%dH2O=%d';
Gesamtergebnis{int16((Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1),int16((Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1),int16((Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1),int16((Betriebsparameter(3,3)-Betriebsparameter(1,3))/Betriebsparameter(2,3)+1)} = [];
for Temp = 1:(Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1%Temperaturschleife
    %T=(Temp-1)*Betriebsparameter(2,2)+Betriebsparameter(1,2);
    for Druck = 1:(Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1%Druckschleife
        %pH2=(Druck-1)*Betriebsparameter(2,1)+Betriebsparameter(1,1);
        for AcO = 1:(Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1%Acetatausgangskonzentrationsschleife
            %Acetat = (AcO-1)*Betriebsparameter(2,4)+Betriebsparameter(1,4);
            for H2O = 1:(Betriebsparameter(3,3)-Betriebsparameter(1,3))/Betriebsparameter(2,3)+1%Wasserausgangskonzentrationsschleife
                %Wasser = (H2O-1)*Betriebsparameter(2,3)+Betriebsparameter(1,3);
                fname = sprintf(Name,Temp,Druck,AcO,H2O);
                load(fullfile('D:\Bachelorarbeit\MatLab\Zwischenerg', fname),'ERG');
                t = ERG{Temp,Druck,AcO,H2O}{1,1};
                c = ERG{Temp,Druck,AcO,H2O}{1,2};
                Gesamtergebnis{Temp,Druck,AcO,H2O} = {t,c}; 
            end
        end
    end
end

end