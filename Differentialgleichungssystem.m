function [dc] = Differentialgleichungssystem(~,c) 
%Konstanten
R  = 8.314461;                            %in J/mol/K
h  = 6.626070*10^(-34);                   %in J*s
kb = 1.380649*10^(-23);                   %in J/K
b  = 68.333333;                           %Setschenow-Konstante in L/mol gemittelt von 25-45�C
%a  = 38.76;                              %Koeffizient f�r Kamlet Taft-Parameter dimlos
%NA = 6.02214076E23;                      %Avogardo-Konstante in 1/mol

%Konzentrationen der Stoffe als Zeilenverktor [mol/L]
cRu         = c(1);
cRuH2       = c(2);
EtOH        = c(3);
cAcH        = c(4);
cEtOAc      = c(5);
cBase        = c(6); 
cKOAc       = c(7);
H2O         = c(8);
cCrAl       = c(9);
cPolymer6   = c(10);
%cPolymer8   = c(11);
cCrOH       = c(12);
cBuAl       = c(13);
cBuOH       = c(14);
T           = c(15);
pH2         = c(16);

[cOH, cEtOH, cH2O] = Ethanolatgg(EtOH, H2O, cBase, T);%Berechnung des Gleichgewichts der Ethanolat+Wasser S�ure-Base-Reaktion
AVerseifung = ((exp(22.2)-exp(19.57))/(2.8)*cH2O+exp(19.57))/60;%Berechnung des Frequenzfaktors der Verseifung in L/mol/s

cSalz =cBase+cKOAc;%Salzkonzentration in mol/L

cges = 14;                                            %Gesamtkonzentration in mol/L
mges = 700;                                           %Gesamtmassenkonzentration in g/L

cH2 = pH2incH2(T,pH2,cges,mges,cBase,cKOAc);          %berechnet cH2 mit Henrykonstante in mol/L
cSalzdiss = Dissozitationsgrad(cH2O,cEtOH,T,cSalz);   %berechnet Konzentration von dissoziiertem Salz (KOH+KOAc) in mol/L
%alpha = KTalpha(cH2O,cEtOH,cBuOH,T);                 %alpha-Kamlet-Taft der L�sung dimlos
%alphaEtOH = KTalpha(0,cEtOH,0,T);                    %alpha-Kamlet-Taft von Ethanol dimlos
%cKOHdiss = Dissozitationsgrad(cH2O,cEtOH,T,cKOH);    %Dissoziationsgrad von KOH

%Aktivierungsbarrieren A [L^2 mol^-2 s^-1] Ea [J mol^-1] G [J mol^-1] H [J mol^-1] S [J mol^-1 K^-1]
Aktivierungsbarrieren = [0              0       95181.99351     25215.19971     -165.9959523	;
                         0              0       97116.98674     88913.4914      -17.46139386	;
                         0              0       73017.52555     -15273.1546     -219.096242     ;
                         0              0       47846.86051     -40903.38464	-193.792769     ;
                         0              0       70247.62343     -21719.33972+25000	-227.1335324	;
                         0              0       87484.02856     1440.53122      -211.0311872	;
                         5.13E13        87000	0               0               0               ;
                         5.13E13        87000	0               0               0               ;
                         5.13E13    	87000	0               0               0               ;
                         AVerseifung	66000	0               0               0               ;
                         0              0       65847.286       -3901.492465	-234.0563037	;
                         0              0       59446.31792     -462.0879365	-201.034919     ;
                         0              0       130754.8881     64308.98816     -222.9728186	;
                         0              0       65267.05062     7918.506912     -192.4447775	;
                         0              0       71948.9472      5306.13477      -223.6335988	;
                         0              0       47941.3785      -10179.0621     -195.0350356	;
];

%Geschwindigkeitskonstanten in 1/s*(L/mol)^n-1
A   = Aktivierungsbarrieren(:,1);
Ea  = Aktivierungsbarrieren(:,2);
H   = Aktivierungsbarrieren(:,4);
S   = Aktivierungsbarrieren(:,5);
k1_1  = kb*T/h*exp(S(1)/R)*exp(-H(1)/(R*T));
k1_2  = kb*T/h*exp(S(2)/R)*exp(-H(2)/(R*T));
k2_1  = kb*T/h*exp(S(3)/R)*exp(-H(3)/(R*T));
k2_2  = kb*T/h*exp(S(4)/R)*exp(-H(4)/(R*T));
k3_1  = kb*T/h*exp(S(5)/R)*exp(-H(5)/(R*T));
k3_2  = kb*T/h*exp(S(6)/R)*exp(-H(6)/(R*T));
k4    = A(7)*exp(-Ea(7)/(R*T))   * exp(b*cSalzdiss*cSalz);  
k5    = A(8)*exp(-Ea(8)/(R*T))   * exp(b*cSalzdiss*cSalz);  
k6    = A(9)*exp(-Ea(9)/(R*T))   * exp(b*cSalzdiss*cSalz);  
k7    = A(10)*exp(-Ea(10)/(R*T)); %* exp(a*(alpha-alphaEtOH));
k8_1  = kb*T/h*exp(S(11)/R)*exp(-H(11)/(R*T));
k8_2  = kb*T/h*exp(S(12)/R)*exp(-H(12)/(R*T));
k9_1  = kb*T/h*exp(S(13)/R)*exp(-H(13)/(R*T));
k9_2  = kb*T/h*exp(S(14)/R)*exp(-H(14)/(R*T));
k10_1 = kb*T/h*exp(S(15)/R)*exp(-H(15)/(R*T));
k10_2 = kb*T/h*exp(S(16)/R)*exp(-H(16)/(R*T));

%Reaktionsgeschwindigkeiten in mol/L/s
r1_1   = k1_1*cRu*cH2;
r1_2   = k1_2*cRuH2;
r2_1   = k2_1*cEtOH*cRu;
r2_2   = k2_2*cAcH*cRuH2;
r3_1   = k3_1*cAcH*cEtOH*cRu;
r3_2   = k3_2*cEtOAc*cRuH2;
r4     = k4*cAcH^2*cBase;                
r5     = k5*cCrAl*cAcH*cBase;
r6     = k6*cPolymer6*cAcH*cBase;
r7     = k7*cEtOAc*cOH;
r8_1   = k8_1*cCrOH*cRu;
r8_2   = k8_2*cCrAl*cRuH2;
r9_1   = k9_1*cBuAl*cRu;
r9_2   = k9_2*cCrAl*cRuH2;
r10_1  = k10_1*cBuOH*cRu;
r10_2  = k10_2*cBuAl*cRuH2;

%Konzentrations�nderungsraten in mol/L/s
dcRu         = -r1_1+r1_2-r2_1+r2_2-r3_1+r3_2-r8_1+r8_2-r9_1+r9_2-r10_1+r10_2;
dcRuH2       = +r1_1-r1_2+r2_1-r2_2+r3_1-r3_2+r8_1-r8_2+r9_1-r9_2+r10_1-r10_2;
dcEtOH       = -r2_1+r2_2-r3_1+r3_2+2*r7;
dcAcH        = r2_1-r2_2-r3_1+r3_2-2*r4-r5-r6;
dcEtOAc      = r3_1-r3_2-r7;
dcBase        = -r7;
dcKOAc       = r7;
dcH2O        = r4+r5+r6-r7;
dcCrAl       = +r4-r5+r8_1-r8_2+r9_1-r9_2;
dcPolymer6   = r5-r6; 
dcPolymer8   = +r6;
dcCrOH       = -r8_1+r8_2;
dcBuAl       = -r9_1+r9_2+r10_1-r10_2;
dcBuOH       = -r10_1+r10_2;

%output Variable definieren in mol/L/s
dc(1,:)  = dcRu;
dc(2,:)  = dcRuH2;
dc(3,:)  = dcEtOH;
dc(4,:)  = dcAcH;
dc(5,:)  = dcEtOAc;
dc(6,:)  = dcBase;
dc(7,:)  = dcKOAc;
dc(8,:)  = dcH2O;
dc(9,:)  = dcCrAl;
dc(10,:) = dcPolymer6;
dc(11,:) = dcPolymer8;
dc(12,:) = dcCrOH;
dc(13,:) = dcBuAl;
dc(14,:) = dcBuOH;
dc(15,:) = 0;
dc(16,:) = 0;
end