profile on
Betriebsparameter = xlsread('Input.xlsx', 'Betriebsparameter');%Import Batriebsparameter und zu variierenden Parameter 
cEing = xlsread('Input.xlsx', 'Ausgangskonzentrationen');%Import der Anfangskonznetrationen
tintv = [Betriebsparameter(1,5) Betriebsparameter(3,5)];%Zeitintervall in s
%tintv = Betriebsparameter(1,5):60:Betriebsparameter(3,5);
options = odeset('NonNegative',1:15);%nur positive L�sungen/Konzentrationen in der L�sung erlaubt
parfor Temp = 1:(Betriebsparameter(3,2)-Betriebsparameter(1,2))/Betriebsparameter(2,2)+1%Temperaturschleife
    T=(Temp-1)*Betriebsparameter(2,2)+Betriebsparameter(1,2);%Temperatur berechnen in K
    for Druck = 1:(Betriebsparameter(3,1)-Betriebsparameter(1,1))/Betriebsparameter(2,1)+1%Druckschleife
        pH2=(Druck-1)*Betriebsparameter(2,1)+Betriebsparameter(1,1);%Dr�cke berechnen in MPa
        for AcO = 1:(Betriebsparameter(3,4)-Betriebsparameter(1,4))/Betriebsparameter(2,4)+1%Acetatausgangskonzentrationsschleife
            Acetat = (AcO-1)*Betriebsparameter(2,4)+Betriebsparameter(1,4);%Acetatkonzentration berechnen in mol/L
            for H2O = 1:(Betriebsparameter(3,3)-Betriebsparameter(1,3))/Betriebsparameter(2,3)+1%Wasserausgangskonzentrationsschleife
                Wasser = (H2O-1)*Betriebsparameter(2,3)+Betriebsparameter(1,3);%Wasserkonzentration berechnen in mol/L
                c0 = [cEing(1,1),cEing(2,1),cEing(3,1),cEing(4,1),cEing(5,1),cEing(6,1),Acetat,Wasser,cEing(9,1),cEing(10,1),cEing(11,1),cEing(12,1),cEing(13,1),cEing(14,1),T,pH2];
                [t, c]=ode23s(@Differentialgleichungssystem, tintv, c0, options);%oder ode23t verwenden
                parsave(Temp,Druck,AcO,H2O,t,c);%Funktion zum Speicher der Zwischenergebnisse               
            end
        end
    end
end
Datensatz = parload();%Funktion zum Erstellen eines 4D-Cell-Arrays mit allen Zwischenergebnissen
save(fullfile('D:\Bachelorarbeit\MatLab', 'Ergebnisse'),'Datensatz','-v7.3');%Dateipfad zum Speicherort + zu speicherende Variable + Version um Datei >2GB zu speichern
profile viewer%gibt Dauer der einelnen Berechnungen an